﻿using System;

namespace TheFootballFest
{
    class Program
    {
        static string prevPlayerID = null;
        static string currPlayerID = null;

        public static void swap()
        {
            string temp = prevPlayerID;
            prevPlayerID = currPlayerID;
            currPlayerID = temp;
        }

        static void Main(string[] args)
        {
            int testCases = 0;
            int totalPass = 0;
            string playerID = null;

            testCases = Convert.ToInt32(Console.ReadLine());

            if ( testCases < 0 || testCases > 100 )
            {
                return;
            }
            for (int testCase = 0; testCase < testCases; testCase++)
            {
                string[] playerInfo = Console.ReadLine().Split(' ');
                totalPass = Convert.ToInt32(playerInfo[0]);
                playerID = playerInfo[1];
                prevPlayerID = "";
                currPlayerID = playerID;
                for (int pass = 0; pass < totalPass; pass++)
                {
                    string[] newPlayerInfo = Console.ReadLine().Split(' ');
                    string passChoice = newPlayerInfo[0];
                    if(passChoice == "P")
                    {
                        playerID = newPlayerInfo[1];                       
                        prevPlayerID = currPlayerID;
                        currPlayerID = playerID;
                    }
                    else if(passChoice == "B")
                    {
                        swap();
                        playerID = prevPlayerID;                       
                    }
                }
                playerID = currPlayerID;
                Console.WriteLine("Player " + playerID);
                Console.ReadKey();
            }
        }
    }
}
