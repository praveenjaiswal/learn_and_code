﻿using System;

namespace littleMonk
{
    class ParenthesisBalance
 
        {
            static void Main(string[] args)
            {
                int[] parenArray;
                int longestParenthesisBalance;
                int sizeOfArray = int.Parse(Console.ReadLine());
                parenArray = new int[sizeOfArray];
                parenArray = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
                CheckBalance CheckBalance = new CheckBalance(sizeOfArray, parenArray);
                longestParenthesisBalance = CheckBalance.GetMaxCount();
                CheckBalance.DisplayLongestParenthesis(longestParenthesisBalance);
              }
         }
         
        class CheckBalance
        {
                int count = 0;
                int maxCount = 0;
                int arrayItem;
                int sizeOfArray;
                int parenArray;
                
                Stack parenStack = new Stack(sizeOfArray);
                
            public CheckBalance(int arraySize, int[] parenthesisArray)
            {
            sizeOfArray = arraySize;
            parenArray = parenthesisArray;          
            }
            
            public int GetMaxCount()
            {
              for (int num = 0; num < sizeOfArray; num++)
                {
                arrayItem = parenArray[num];
                    if (parenStack.isEmpty())
                    {
                        if (arrayItem > 0)
                        {
                            parenStack.push(arrayItem);
                        }
                    }
                    else
                    {
                        int stackItem = parenStack.peek();
                        if (stackItem > arrayItem && (stackItem + arrayItem) == 0)
                        {
                            count = count + 2;
                            if (count > maxCount)
                            {
                                maxCount = count;
                            }
                            parenStack.pop();
                        }
                        else
                        {
                            parenStack.push(arrayItem);
                            count = 0;
                        }
                    }//endOfElse
                }//endOfFor
                return maxCount;
            }//endOfGetMaxCount
            
            public void DisplayLongestParenthesis(int longestParenthesisBalance)
            {
                Console.WriteLine(longestParenthesisBalance);
            }
        }//end Of CheckBalance Class
    }



       
  
        
    