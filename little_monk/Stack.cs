﻿
namespace ConsoleApplication8
{
    public class Stack
    {
        int[] array;
        int top;
        public Stack(int n)
        {
            array = new int[n];
            top = -1;
        }
        public void push(int t)
        {
            top = top + 1;
            array[top] = t;
        }
        public int pop()
        {
            int topElement = array[top];
            top = top - 1;
            return topElement;
        }
        public int peek()
        {
            if (top >= 0)
            {
                return array[top];
            }
            else
            {
                return 0;
            }
        }
        public int topIndexValue()
        {
            return top;
        }
        public bool isEmpty()
        {
            if (top < 0)
                return true;
            else
                return false;
        }

    }
}
